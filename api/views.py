import json
import os
import hmac
import hashlib
from pathlib import Path
from ntpath import join
from dotenv.main import load_dotenv
from rest_framework import status
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

#get authkey secret
BASE_DIR = Path(__file__).resolve().parent.parent
dotenv_path = join(BASE_DIR, '.env')
load_dotenv(dotenv_path)
SECRET_AUTHKEY = os.environ.get("SECRET_AUTHKEY")

# Create your views here.
@csrf_exempt
def calculate_ranking(request):
    if request.method == 'POST':
        #try get authkey, error if not provided
        try:
            authkey = request.headers['auth']
        except:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

        #verify authkey format
        is_authkey_valid = verify_authkey(authkey=authkey, request_body=request.body)
        if (not is_authkey_valid):
            return HttpResponse({'Wrong Authkey'},status=status.HTTP_401_UNAUTHORIZED)

        #load request data
        data = json.loads(request.body)

        #calculate ranking
        try:
            ranks = []
            existing_scores = data['existingScores']
            new_scores = data['newScores']
            existing_scores.sort(reverse=True)

            # # number 1: insert one then evaluate rank, repeat for each new score
            # for score in new_scores:
            #     current_rank = 1
            #     previous_looped_score = float('inf')
            #     for i in range(len(existing_scores)):
            #         if (score >= existing_scores[i]):
            #             ranks.append(current_rank)
            #             existing_scores.insert(i, score)
            #             break
            #         elif (score < existing_scores[i]):
            #             #if loop reach end, then insert
            #             if i == (len(existing_scores) - 1):
            #                 current_rank += 1
            #                 ranks.append(current_rank)
            #                 existing_scores.append(score)

            #             #compare current loop score to previous loop score
            #             #if previous larger, then current score rank is changed
            #             if previous_looped_score > existing_scores[i]:
            #                 current_rank += 1
            #             previous_looped_score = existing_scores[i]

            # number 2: insert all new scores, then evaluate their ranks
            for score in new_scores:
                for i in range(len(existing_scores)):
                    if (score >= existing_scores[i]):
                        existing_scores.insert(i, score)
                        break
                    elif (score < existing_scores[i]):
                        if i == (len(existing_scores) - 1):
                            existing_scores.append(score)
            
            for score in new_scores:
                current_rank = 1
                previous_looped_score = float('inf')
                for i in range(len(existing_scores)):
                    if (score == existing_scores[i]):
                        ranks.append(current_rank)
                        break
                    if (score < existing_scores[i]):
                        if previous_looped_score > existing_scores[i]:
                            current_rank += 1
                        previous_looped_score = existing_scores[i]
        except:
            return HttpResponse(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        #forms response data
        response_data = {}
        response_data['ranks'] = ranks
        response_data['resultScores'] = existing_scores
        contents = json.dumps(response_data)

        #return response
        return HttpResponse(
            content=contents,
            content_type='application/json',
            status=status.HTTP_200_OK
        )
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

@csrf_exempt
def cipher(request):
    if request.method == 'POST':
        #try get authkey, error if not provided
        try:
            authkey = request.headers['auth']
        except:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

        #verify authkey format
        is_authkey_valid = verify_authkey(authkey=authkey, request_body=request.body)
        if (not is_authkey_valid):
            return HttpResponse({'Wrong Authkey'},status=status.HTTP_401_UNAUTHORIZED)

        #load request data
        data = json.loads(request.body)

        #vigenere cipher
        #define character value
        character = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

        text = []

        #check is mode valid
        mode = data['mode']
        if (mode != 'enkripsi' and mode != 'dekripsi'):
            return HttpResponse({'Invalid cipher mode'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        cipher_message = data['teks']
        cipher_key = data['key']
        whitespace_offset = 0

        for i in range(len(cipher_message)):
            #if whitespace, do not encrypt and key position not increased
            if (cipher_message[i].isspace()):
                whitespace_offset -= 1
                text.append(cipher_message[i])
                continue

            message_character_num = character.index(cipher_message[i])
            cipher_key_index = (i + whitespace_offset) % len(cipher_key)
            key_character_num = character.index(cipher_key[cipher_key_index])

            cipher_character_num = 0
            if (mode == 'enkripsi'):
                cipher_character_num = (message_character_num + key_character_num) % len(character)
            elif (mode == 'dekripsi'):
                cipher_character_num = (message_character_num - key_character_num) % len(character)
            
            cipher_character = character[cipher_character_num]
            text.append(cipher_character)

        str_text = listToString(text)

        print(str_text)

        #forms response data
        response_data = {}
        response_data['text'] = str_text
        contents = json.dumps(response_data)

        #return response
        return HttpResponse(
            content=contents,
            content_type='application/json',
            status=status.HTTP_200_OK
        )
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

def verify_authkey(authkey, request_body):
    message = request_body.decode('utf-8')
    message = ''.join(message.split())
    byte_key = bytes(SECRET_AUTHKEY, 'utf-8')
    encoded_message = message.encode('utf-8')
    h = hmac.new(byte_key, encoded_message, hashlib.sha256).hexdigest()

    return authkey == h

def listToString(s): 
    # from https://www.geeksforgeeks.org/python-program-to-convert-a-list-to-string/
    # initialize an empty string
    str1 = "" 
    
    # traverse in the string  
    for ele in s: 
        str1 += ele  
    
    # return string  
    return str1 
    
