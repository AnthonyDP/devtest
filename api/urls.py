from django.urls import path
from api import views


app_name = 'api'

urlpatterns = [
    path('calculate/ranking/', views.calculate_ranking, name='calculate_ranking'),
    path('cipher/', views.cipher, name='cipher'),
]
