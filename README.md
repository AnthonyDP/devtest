# Setup
Proses untuk menjalankan proyek di lokal (sistem operasi Windows)

1.  Clone Repository
    <pre>git clone https://gitlab.com/AnthonyDP/devtest.git</pre>
<br>

2. Membuat Virtual Environment <br>
Masuk ke dalam folder yang sudah di-clone, kemudian jalankan perintah berikut di command line
    <pre>python -m venv env</pre>
    Kemudian masuk kedalam virtual environment dengan menjalankan script "activate" yang berada pada path berikut
    <pre>env\Scripts\activate</pre>
<br>

3. Install Dependencies<br>
Untuk install dependencies, dapat dilakukan dengan pip install
    <pre>pip install -r requirements.txt</pre>
<br>

4. Membuat .env<br>
Buat file dengan nama '.env', dan masukkan text ini kedalam file
    <pre>
    SECRET_KEY=%insert your secret%
    SECRET_AUTHKEY=%insert your secret authkey%
    </pre>
<br>

5. Run Application<br>
    <pre>
    python manage.py runserver
    </pre>

6. Collection API
    collection API bisa didapatkan dari file "devTest.postman_collection.json"
